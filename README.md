# The Wonderful World of Embedded Computing 

The documentation in the CBA's **pub/hello-world** world is shotgun examples of how to use our favourite microcontrollers.

Many others use extensive libraries to abstract hardware functionality into easy to read code (i.e. arduino, or STM32's HAL). We tend to avoid this - this is something of an aesthetic choice, but also has [measurable performance benefits](https://pub.pages.cba.mit.edu/ring/). 

For those just entering the world of embedded programming, there are a few useful concepts to have a handle on before beginning.

## Toolchains, Programmers

Getting code onto a microcontroller is no small feat! 

The end result is that some block of memory in the micro contains the program instructions we have written. A few things need to happen for this to become true.

![toolchain](images/general-tool-chain.png)

`` C Code -> Compiled Instructions -> Memory Block (.hex, .bin, .elf) -> Programmer -> Micro ``

### Compiling Code

Typically, we'll see systems use GCC (the GNU Compiler Collection) which is a command-line tool available in most systems. If we use another IDE (say, Atmel Studio or Arduino), these IDEs will actually be calling GCC when they make your code. With 'makefile' systems, when we run 'make flash' or 'make program' etc, one of the things we are doing is invoking GCC to compile the program.

A compiler needs to act a little bit differently given different processor architectures. I.E. AVR chips != ARM chips != Intel, because these all have different instruction sets. We need, at some point, to tell our compiler which device it is building for.

### Using a Programmer to Load Code

Now that we have a program, we'll need to push it into the memory of the chip. There are a few different protocols to do this.

 - AVR ISP
 - AVR PDI
 - JTAG
 - SWDIO

Depending on your chip, you'll need to find a programmer (device, i.e. the Atmel ICE, USBISP, etc) - these are typically USB devices (i.e. they can talk to a PC) that 'convert' program memory files into a datastream that the microcontroller will load into its memory.

## Registers, Peripherals, oh my

While microcontrollers *include* tiny processors, they interface with the world with auxilliary circuits called **peripherals**. We can think of these like little bundled ASICs (application specific integrated circuits) that offload time-sensitive work from the processor. These also perform level shifting and current carrying capability.

![die](images/XC2C32A_die.jpg)

Here's the diagram for an XMEGA

![xmega](images/xmega-blocks.png)

Some common peripherals:
 - [PORT / PIN for digital logic (high, low, input)](https://www.instructables.com/id/ATTiny-Port-Manipulation/)
 - [UART (universal asynchronous receive transmit)](https://learn.sparkfun.com/tutorials/serial-communication/uarts)
 - [SPI (serial perhiperal interface)](https://learn.sparkfun.com/tutorials/serial-peripheral-interface-spi/all)
 - [I2C (addressable two-wire interface)](https://learn.sparkfun.com/tutorials/i2c)
 - ADC (analog to digital converter)
 - DAC (digital to analog converter)
 - PWM / TC (timer counter - powerful! useful! can do anything!)

If we imagine peripherals to be little machines that the processor operates, we can think of registers as *memory locations whose bits are switches* for those machines. This is a bit odd, but I find it a useful metaphor. When we write into register locations, we are switching these bits on and off.

![handles](images/register.png)

So! Figuring out which registers to read / write to is where we come to the datasheet. These provide extensive information on which registers do what, what order they need to be configured in, etc. 

[**REGISTER MANIPULATION TUTORIAL**](https://home.roboticlab.eu/en/avr/registers)

[**Bitwise Operators**](https://en.wikipedia.org/wiki/Bitwise_operations_in_C)

## Reading Datasheets

Is a PITA, but hugely enabling. Here are two guides: [one](https://www.egr.msu.edu/classes/ece480/capstone/read_datasheet.pdf) and [two](https://www.sparkfun.com/tutorials/223). 

## CBA Examples 

 - [XMEGA: friendly, powerful](https://gitlab.cba.mit.edu/pub/hello-world/xmega)
 - [NRF52: native wireless, fast ARM-core](https://gitlab.cba.mit.edu/pub/hello-world/nrf52)
 - [ATSAMD51: 120MHz ARM M4, Adafruit Love](https://gitlab.cba.mit.edu/pub/hello-world/atsamd51)
 - [ATSAMS70: 300MHz ARM M7](https://gitlab.cba.mit.edu/pub/hello-world/atsams70)

## Other Architectures

### PSOCs

Programmable Systems On a Chip are somewhere between FPGAs and Microcontrollers. They typically contain a microcontroller core with adaptable / configurable sets of peripherals. Very cool. Graphical language mixed with c code.

![psoc](images/psoc.png)

### FPGAs 

FPGAs are reconfigurable everything-s. 

![fpga](images/fpga-architecture.png)

[tinyfpga](https://tinyfpga.com/)
[fpga4fun](https://www.fpga4fun.com/)
[tinyfpga experiment](https://gitlab.cba.mit.edu/jakeread/coclocking)